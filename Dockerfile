FROM python:3.12

# install dependencies
RUN apt-get update && \
    apt-get install -y less

# set working directory
WORKDIR /app

# Install the application dependencies
RUN wget "https://github.com/marbl/Krona/releases/download/v2.7.1/KronaTools-2.7.1.tar" && \
    tar -xvf KronaTools-2.7.1.tar && \
    cd KronaTools-2.7.1 && \
    ./install.pl --prefix .

COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt

# Copy the application to the server
COPY SuomiInsectPROTAX ./SuomiInsectPROTAX
COPY config.py .
COPY app ./app

# add permissions
RUN chmod -R a+rx SuomiInsectPROTAX && \
    mkdir tmp && chmod 777 tmp

# start app
EXPOSE 8000
CMD gunicorn --threads 3 --timeout 18000 -b 0.0.0.0:8000 app:app
