from enum import Enum


class ProtaxModel(Enum):
    COIFULL = 'modelCOIfull'
    COILERAY = 'modelCOIleray'
