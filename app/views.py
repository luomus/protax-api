from app import app, request_logger
from flask import request, abort, Response
import os
import shutil
import subprocess
import tempfile
from app.models import ProtaxModel
import time


@app.route('/')
def index():
    return 'protax api'


@app.route('/analyse', methods=['POST'])
def analyse():
    request_data = get_request_data()
    if request_data is None:
        make_request_log_entry(False)
        return abort(400)

    prog_folder = os.path.join(app.config['PROTAX_FOLDER'], 'prog')
    model_folder = os.path.join(app.config['PROTAX_FOLDER'], request_data['model'].value)
    os.makedirs(app.config['TMP_FOLDER'], exist_ok=True)
    tmp_folder = tempfile.mkdtemp(dir=app.config['TMP_FOLDER'])
    output_folder = os.path.join(tmp_folder, 'output')
    os.makedirs(output_folder)

    formatted_file = os.path.join(tmp_folder, 'tmp_aligned.fa')
    protax_file = os.path.join(output_folder, 'output.txt')
    krona_file = os.path.join(output_folder, 'protax.html')

    start_time = time.time()

    format_input_data(request_data['data'], prog_folder, model_folder, tmp_folder, formatted_file)
    if not os.path.exists(formatted_file):
        make_request_log_entry(False, time.time() - start_time, None, request_data['threshold'], request_data['min_overlap'])
        shutil.rmtree(tmp_folder)
        return abort(400)

    sequence_count = count_sequences(formatted_file)

    analyse_sequence(formatted_file, prog_folder, model_folder, request_data['threshold'], request_data['min_overlap'], protax_file)
    os.remove(formatted_file)
    create_krona_file(protax_file, prog_folder, model_folder, tmp_folder, krona_file)

    zip_file = shutil.make_archive(output_folder, 'zip', output_folder)
    shutil.rmtree(output_folder)

    make_request_log_entry(True, time.time() - start_time, sequence_count, request_data['threshold'], request_data['min_overlap'])
    return stream_response(zip_file, tmp_folder)


def get_request_data():
    if 'data' not in request.files:
        return None

    data = request.files['data']
    filename = data.filename

    if filename == '':
        return None

    file_ext = os.path.splitext(filename)[-1]
    if file_ext not in app.config['UPLOAD_EXTENSIONS']:
        return None

    model = request.form.get('model', ProtaxModel.COIFULL, type=ProtaxModel)
    threshold = request.form.get('probabilityThreshold', 0.1, type=float)
    if threshold < 0 or threshold > 1:
        return None
    min_overlap = request.form.get('minimumOverlap', 100, type=int)
    if min_overlap < 1 or min_overlap > 500:
        return None

    return {
        'data': data,
        'model': model,
        'threshold': threshold,
        'min_overlap': min_overlap
    }


def stream_response(zip_file, tmp_folder, chunk_size=8192):
    def generate():
        try:
            with open(zip_file, 'rb') as fd:
                while True:
                    buf = fd.read(chunk_size)
                    if buf:
                        yield buf
                    else:
                        break
        finally:
            shutil.rmtree(tmp_folder)

    r = Response(generate(), mimetype='application/zip')
    r.headers.add('Content-Disposition', 'attachment; filename=output.zip')
    r.headers.add('Content-Length', str(os.path.getsize(zip_file)))
    return r


def format_input_data(data, prog, model, tmp_folder, output_file):
    input_file = os.path.join(tmp_folder, 'input_data.fa')
    tmp_file = os.path.join(tmp_folder, 'tmp.a2m')

    data.save(input_file)
    subprocess.call([
        os.path.join(prog, 'hmmalign'),
        '--outformat',
        'A2M',
        '-o',
        tmp_file,
        os.path.join(model, 'refs.hmm'),
        input_file
    ])

    os.remove(input_file)

    if os.path.getsize(tmp_file) == 0:
        return

    subprocess.call([
        'perl',
        os.path.join(prog, 'removeinserts.pl'),
        tmp_file
    ], stdout=open(output_file, 'w'))

    os.remove(tmp_file)


def analyse_sequence(formatted_file, prog, model, threshold, min_overlap, output_file):
    subprocess.call([
        os.path.join(prog, 'classify_mxcode_new'),
        os.path.join(model, 'taxonomy.priors'),
        os.path.join(model, 'refs.aln'),
        os.path.join(model, 'model.rseqs.numeric'),
        os.path.join(model, 'model.pars'),
        os.path.join(model, 'model.scs'),
        '{}'.format(threshold),
        '{}'.format(min_overlap),
        formatted_file
    ], stdout=open(output_file, 'w'))


def create_krona_file(result_file, prog, model, tmp_folder, output_file):
    xml_file = os.path.join(tmp_folder, 'protax2krona.xml')

    subprocess.call([
        'perl',
        os.path.join(prog, 'mx_protax2kronaxml.pl'),
        '0.9',
        'Arthropoda',
        os.path.join(model, 'taxonomy.priors'),
        result_file
    ], stdout=open(xml_file, 'w'))

    subprocess.call([
        os.path.join(app.config['KRONA_FOLDER'], 'bin', 'ktImportXML'),
        '-o',
        output_file,
        xml_file
    ])

    os.remove(xml_file)


def make_request_log_entry(success, exec_time=None, sequence_count=None, threshold=None, min_overlap=None):
    request_logger.info('', extra={
        'ip_address': get_request_ip_address(),
        'success': success,
        'exec_time': exec_time,
        'sequence_count': sequence_count,
        'threshold': threshold,
        'min_overlap': min_overlap
    })


def count_sequences(input_file):
    count = 0
    with open(input_file) as fh:
        for line in fh:
            if line[0] == '>':
                count += 1
    return count


def get_request_ip_address():
    if request.environ.get('HTTP_X_FORWARDED_FOR') is None:
        return request.environ['REMOTE_ADDR']
    else:
        # behind a proxy
        return request.environ['HTTP_X_FORWARDED_FOR'].split(',')[0]
