from flask import Flask
from config import Config
import logging
import os

app = Flask(__name__)
app.config.from_object(Config)

os.makedirs(app.config['LOG_FOLDER'], exist_ok=True)
handler = logging.FileHandler(os.path.join(app.config['LOG_FOLDER'], 'error.log'))
handler.setLevel(logging.ERROR)
app.logger.addHandler(handler)

formatter = logging.Formatter('%(asctime)s %(ip_address)s %(success)s %(exec_time)s %(sequence_count)s %(threshold)s %(min_overlap)s')
handler = logging.FileHandler(os.path.join(app.config['LOG_FOLDER'], 'request.log'))
handler.setFormatter(formatter)
request_logger = logging.getLogger('request')
request_logger.setLevel(logging.INFO)
request_logger.addHandler(handler)

from app import views
