import os


class Config(object):
    PROTAX_FOLDER = 'SuomiInsectPROTAX'
    KRONA_FOLDER = os.environ.get('KRONA_FOLDER', '/opt/KronaTools-2.7.1')
    TMP_FOLDER = 'tmp'
    LOG_FOLDER = 'logs'

    UPLOAD_EXTENSIONS = ['.fa']
