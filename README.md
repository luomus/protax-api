# Protax Api #

Api for the Protax Insects program. The program identifies finnish insects from DNA sequences.

## Endpoints
### /analyse (POST)
Takes a fasta file, probability threshold and model name as a parameters.

Returns a zip file that contains two files:

- output.txt: Protax Insects program results
- CMTPC.html: Krona chart of the results

## Development

First download the SuomiInsectPROTAX program from the [wiki](https://wiki.helsinki.fi/display/luomusict/Protax+api) and put it inside the project folder. Wiki also contains examples that can be used for testing.

### Run the app with docker

Install docker and docker-compose and run the app with commands:

    docker-compose build
    docker-compose up

### Run flask development server

1. Create Python virtualenv and install requirements

        virtualenv -p python3 env
        source env/bin/activate
        pip install -r requirements.txt


2. Install KronaTools (KronaTools should be installed somewhere else than the project directory because otherwise openshift build doesn't work)

        cd /opt
        sudo wget "https://github.com/marbl/Krona/releases/download/v2.7.1/KronaTools-2.7.1.tar"
        sudo tar -xvf KronaTools-2.7.1.tar
        sudo rm KronaTools-2.7.1.tar
        cd KronaTools-2.7.1
        sudo ./install.pl --prefix .

3. Run development server

        flask run --port=8080
        
## Deployment

Deploy to Openshift with oc:

    oc process -f openshift/template.yaml -p APP=protax-api-dev -p HOST=protax-api-dev.laji.fi | oc create -f -
    oc start-build protax-api-dev-nginx --from-dir=./nginx --follow
    oc start-build protax-api-dev --from-dir=./ --follow
        
If you want to delete all the resources (for example if you want to remove the dev version to save resources), you can use these commands:

    oc delete all --selector app=protax-api-dev
    oc delete pvc --selector app=protax-api-dev
        
The production version can be deployed using the same commands if you remove "-dev" from them.

